#www-team1-min-bz

![logo](assets/minbuza.jpg) 

## Summary
Dit is de repository voor de wordpress website van het ministerie van buitenlandse zaken.

## Deployment
De server is dusdanig ingericht dat alle commits en merges met de ***master*** branch automatisch doorgevoerd worden naa productie. Pas dus op met het toevoegen van nieuwe functionalieten op de master branch.

